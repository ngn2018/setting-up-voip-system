# SIP Server Installation and Creating Users #

This README  is documented for setting up asterisk server and users.

### Setting up asterisk server ###

* Version :
Asterisk 13.18.3

* Installing Asterisk server :

sudo apt-get install asterisk

or this command , depending upon distribution.

yum install asterisk

### Creating Users ###

Type the following commands to edit the user configuration file

sudo vim /etc/asterisk/users.conf

To create users insert the following commands into the very end of the vim file . press 'i' to edit in the vim file. then insert the following block :

[6001]
fullname = Example Alice
secret = 1234
hassip = yes
context = users
host = dynamic

[6002]
fullname = Example Bob
secret = 1234
hassip = yes
context = users
host = dynamic

To understand the syntax please refer to link in last section.

To save the vim file , press Esc and then :x (the colon and then x).

* Saving file in vim:
The cursor will go to the bottom of the screen at a colon prompt. Write your file by entering :w and quit by entering :q . You can combine these to save and exit by entering :wq or :x

Now we have to insert the users in extensions file of asterisk server. insert the following command in the console:

sudo vim /etc/asterisk/extensions.conf

In the end of the file insert the users extensions (process of insetring and saving in vim file have been discussed above)

exten => 6001,1,Dial(SIP/6001)

exten => 6002,1,Dial(SIP/6002)

save the vim file.

After then reload the asterisk server and view users which are added:

sudo asterisk -rx "reload"
sudo asterisk -rx "sip show users"

### User side app installation ###

CSipSimple is an OpenSource SIP app used in our case. install CSipSimple app from Google Playstore. 

* Version :
CSipSimple 1.02.03

Open the app. goto add account.  choose wizard " basic".

For "Account name" give your name

for "User" give the username specified in sipserver e.g  in our case we select '6001'

for Server , give ip of sip server

for password, enter the password you had given in sip server, e.g in our case it was 12345678

Once youve set the above attributes , save the account setting.

For testing create another user say '6002' in another phone/tab and then call one of the user i.e. 6001 calling to 6002.
Once call is established you  will see SIP is working.


### Contribution guidelines ###
https://mike42.me/blog/2015-01-02-how-to-set-up-asterisk-in-10-minutes

https://www.voip-info.org/asterisk-cli/

### Who do I talk to? ###
Please post your queries, if you have one.

* Mamoona Aslam